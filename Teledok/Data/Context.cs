﻿using Microsoft.EntityFrameworkCore;
using Teledok.Models;

namespace Teledok.Data
{
	public class Context : DbContext
	{
		public Context(DbContextOptions<Context> options) :  base(options)
		{
			Database.EnsureCreated();
		}

		public DbSet<Client> Clients { get; set; }

		public DbSet<Founder> Founders { get; set; }
	}
}
