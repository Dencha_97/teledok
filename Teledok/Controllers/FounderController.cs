﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Teledok.Data;
using Teledok.Models;

namespace Teledok.Controllers
{
	public class FounderController : Controller
	{
		private readonly Context _context;

		public FounderController(Context context)
		{
			_context = context;
		}

		public IActionResult Founder()
		{
			return View(_context.Founders.ToList());
		}

		public IActionResult Details(int id)
		{
			Founder founder = _context.Founders.Where(x => x.FounderID == id).FirstOrDefault();
			return View("DetailsFounder", founder);
		}

		[HttpGet]
		public IActionResult Create()
		{
			Founder founder = new Founder();
			var clients = _context.Clients.ToList();
			return View("CreateFounder");
		}

		[HttpPost]
		public IActionResult Create(Founder founder)
		{
			if (ModelState.IsValid)
			{
				founder.ClientID = _context.Clients.Where(x=>x.Name == founder.Client).Select(x=>x.ClientID).FirstOrDefault();
				founder.DateAdd = DateTime.Now;
				founder.DateRefresh = DateTime.Now;
				_context.Add(founder);
				_context.SaveChanges();
				return RedirectToAction("Founder");
			}

			return View("CreateFounder");
		}
	}
}
