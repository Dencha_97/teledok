﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace Teledok.Models
{
	public class Client
	{
		[Key]
		public int ClientID { get; set; }

		[BindProperty]
		[Required(ErrorMessage = "Незаполнен ИНН")]
		[Display(Name = "ИНН")]
		public int INN { get; set; }

		[BindProperty]
		[Required(ErrorMessage = "Незаполнено наименование")]
		[Display(Name = "Наименование")]
		public string Name { get; set; }

		[BindProperty]
		[Required(ErrorMessage = "Незаполнен тип")]
		[Display(Name = "Тип (ЮР/ИП)")]
		public string TypeClient { get; set; }

		[Display(Name = "Дата добавления")]
		public DateTime DateAdd { get; set; }

		[Display(Name = "Дата обновления")]
		public DateTime DateRefresh { get; set; }
	}
}
