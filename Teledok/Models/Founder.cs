﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Teledok.Models
{
	public class Founder
	{
		[Key]
		public int FounderID { get; set; }

		[Required(ErrorMessage = "Незаполнено ФИО")]
		[Display(Name = "ФИО")]
		public string FIO { get; set; }
		
		[Display(Name = "Дата добавления")]
		public DateTime DateAdd { get; set; }
		
		[Display(Name = "Дата обновления")]
		public DateTime DateRefresh { get; set; }

		
		[ForeignKey(nameof(ClientID))]
		public int ClientID { get; set; }

		[Required(ErrorMessage = "Незаполнен клиент")]
		[Display(Name = "Клиент")]
		public string Client { get; set; }
	}
}
