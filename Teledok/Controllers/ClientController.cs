﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using Teledok.Data;
using Teledok.Models;

namespace Teledok.Controllers
{
	public class ClientController : Controller
	{
		private readonly Context _context;

		public ClientController(Context context)
		{
			_context = context;
		}

		public IActionResult Client()
		{
			return View(_context.Clients.ToList());
		}

		public IActionResult Details(int id)
		{
			Client client = _context.Clients.Where(x => x.ClientID == id).FirstOrDefault();
			return View("DetailsClient",client);
		}

		[HttpGet]
		public IActionResult Create()
		{
			return View("CreateClient");
		}

		[HttpPost]
		public IActionResult Create(Client client)
		{
			if (ModelState.IsValid)
			{
				client.DateAdd = DateTime.Now;
				client.DateRefresh = DateTime.Now;
				_context.Add(client);
				_context.SaveChanges();
				return RedirectToAction("Client");
			}

			return View("CreateClient");
		}
	}
}
